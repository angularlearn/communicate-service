import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class MissionService {

  // Observable string sources
  private missionAnnouncedSubj = new Subject<string>();
  private missionConfirmedSubj = new Subject<string>();
  private loremInfoSubj = new Subject<string>();

  // Observable string streams
  missionAnnouncedSubj$ = this.missionAnnouncedSubj.asObservable();
  missionConfirmedSubj$ = this.missionConfirmedSubj.asObservable();
  loremInfoSubj$ = this.loremInfoSubj.asObservable();

  // Service message commands
  announceMission(mission: string) {
    this.missionAnnouncedSubj.next(mission);//принимает миссию
  }

  confirmMission(astronaut: string) {
    this.missionConfirmedSubj.next(astronaut);//принимает астронавта
  }

  loremInfo(info: string) {
    this.loremInfoSubj.next(info);
  }

}
