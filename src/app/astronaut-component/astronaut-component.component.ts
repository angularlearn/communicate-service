import { Component, Input, OnDestroy } from '@angular/core';
import { MissionService } from '../mission.service';
import { Subscription }   from 'rxjs/Subscription';
@Component({
  selector: 'my-astronaut',
  templateUrl: './astronaut-component.component.html',
  styleUrls: ['./astronaut-component.component.css']
})
export class AstronautComponentComponent implements OnDestroy {

  @Input() astronaut: string;
  mission = '<no mission announced>';
  confirmed = false;
  announced = false;
  subscription: Subscription;

  constructor(private missionService: MissionService) {

    this.subscription = missionService.missionAnnouncedSubj$.subscribe(//подписка, получил миссию - выполнился метод missionService.announceMission()
      (mission) => {
        this.mission = mission;
        this.announced = true;
        this.confirmed = false;
    });

    this.subscription = missionService.loremInfoSubj$.subscribe(
      (info) => {
        console.log(info);
    });

    console.log(this.subscription);
  }

  confirm() {
    this.confirmed = true;
    this.missionService.confirmMission(this.astronaut); //передали астронавта в сервис
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
  }
}
