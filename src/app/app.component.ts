import { Component } from '@angular/core';
import { MissionService } from './mission.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MissionService]
})
export class AppComponent {
  astronauts = ['Lovell', 'Swigert', 'Haise'];
  history: string[] = [];
  missions = ['Fly to the moon!',
              'Fly to mars!',
              'Fly to Vegas!'];
  nextMission = 0;
  info = "Lorem ipsum set dolor"

  constructor(private missionService: MissionService) {//заинджектил сервис

    missionService.missionConfirmedSubj$.subscribe(//подписка, получил астронавта
      (astronaut) => {
        this.history.push(`${astronaut} confirmed the mission`);
      });
  }


  announce() {
    let mission = this.missions[this.nextMission++];

    this.missionService.announceMission(mission);//передал миссию в сервис

    this.missionService.loremInfo(this.info);



    this.history.push(`Mission "${mission}" announced`);
    if (this.nextMission >= this.missions.length) { this.nextMission = 0; }
  }


}
